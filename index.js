// khai báo thư viện cần dùng
const express = require("express");
const mongoose = require('mongoose');
const app = express();

// Khai báo để sử dụng body json
app.use(express.json());

//routers
const drinkRouter = require("./app/router/drinkRouter");
const orderRouter = require("./app/router/orderRouter");
const userRouter = require("./app/router/userRouter");
const voucherRouter = require("./app/router/voucherRouter");
const devcampPizza365Router = require("./app/router/devcampPizza365Router")

app.use("", drinkRouter);
app.use("", orderRouter);
app.use("", userRouter);
app.use("", voucherRouter);
app.use("", devcampPizza365Router);

// khi GET trả về trang chủ
app.use(express.static(__dirname + "/views"));
app.get("/", function(request, response) {
    response.sendFile(__dirname + "/views/index.html")
})

// khai báo cổng
const port = 8000;
app.listen(port, function() {
    console.log(`App listening on port ${port}`);
})

// kết nối mongoDB
const uri = `mongodb://127.0.0.1:27017/devcamp-pizza365`
mongoose.connect(uri, function(error) {
    if (error) throw error;
    console.log(`Successfully connected to ${uri}`);
})
   