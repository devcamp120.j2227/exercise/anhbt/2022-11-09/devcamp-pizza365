// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

//Import review controller 
const orderController = require("../controllers/orderController");

router.post("/order", orderController.createOrder);

router.get("/order", orderController.getAllOrder);

router.get("/order/:orderId", orderController.getOrderByID);

router.put("/order/:orderId", orderController.updateOrderByID);

router.delete("/order/:orderId", orderController.deleteOrderById);
module.exports = router;