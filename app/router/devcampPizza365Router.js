// khai báo thư viện express js
const express = require("express");

//khai báo router app
const router = express.Router();

// khai báo controller
const  devcampPizza365Controller = require("../controllers/devcampPizza365Controller");

router.post("/devcamp-pizza365/orders", devcampPizza365Controller.createOrder)
router.get("/devcamp-pizza365/voucher_detail/:maVoucher", devcampPizza365Controller.getVoucherDetail)
module.exports = router;
