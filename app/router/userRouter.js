// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

//Import review controller 
const userController = require("../controllers/userController");


router.get("/user", userController.getAllUser);
router.get("/limit-users", userController.getLimitUsers);
router.get("/skip-users", userController.getSkipUsers);
router.get("/sort-users", userController.getSortUsers);
router.get("/skip-limit-users", userController.getSkipLimitUsers);
router.get("/sort-skip-limit-users", userController.getSortSkipLimitUsers);


router.post("/user", userController.createUser );

router.get("/user/:userId", userController.getUserByID);

router.put("/user/:userId", userController.updateUserByID);

router.delete("/user/:userId", userController.deleteUserById);
module.exports = router;