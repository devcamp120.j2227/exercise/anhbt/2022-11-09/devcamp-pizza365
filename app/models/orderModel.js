//import thư viện moongose
const mongoose = require("mongoose");

//class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance order Schema từ class schema
const orderSchema  = new Schema({
    orderCode: {
        type: String,
        unique: true, 
        default: function(){
            var length = 5;
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }
    },
    pizzaSize:{
        type:String,
        require: true
    },
    pizzaType:{
        type:String,
        require: true,
    },
    voucher:{
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink:{
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    user:{
        type: mongoose.Types.ObjectId,
        ref: "User"
    },
    status: 
    {
        type:String,
        require: true
    }
},{
    timestamps:true
})
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Order", orderSchema )