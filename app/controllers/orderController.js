//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const orderModel = require("../models/orderModel")

const createOrder = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "pizzaSize" : "S",
    //     "pizzaType" : "Bacon",
    //     "status": "inQueue",
    //      "user" : ,
    // }

    // Bước 2: validate dữ liệu
    if(!body.pizzaSize){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "PizzaSize không hợp lệ" 
        })
    }
    if(!body.pizzaType){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "PizzaType không hợp lệ" 
        })
    }
    if(!body.status){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Status không hợp lệ" 
        })
    }
    if(!body.user){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "user không hợp lệ" 
        })
    }
    
    // bước 3: Gọi Model tạo dữ liệu
    const newOrder = {
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status,
        user: body.user
    };
    //bước 4: trả về kết quả 
    orderModel.create(newOrder, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create Order: Successfull",
            data: data
        })
    } );
}
const getAllOrder  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    orderModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all order: Successfull",
            data: data
        })
    })
}
const getOrderByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const orderID =  request.params.orderId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderID)){
     return response.status(400).json({
             status: "Bad request",
             message: "Order Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     orderModel.findById(orderID,(error,data) =>{
         return response.status(200).json({
             status: "Get Detail Order successfull",
             data: data
             })
         }
     )  
 }
 const updateOrderByID = (request,response) => {
    // bước 1: chuẩn bị dữ liệu
    const orderId =  request.params.orderId;
    const body = request.body;

   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(orderId)){
    return response.status(400).json({
        status: "Bad request",
        message: "orderId không hợp lệ" 
    })
    }
    if(body.pizzaSize !== undefined && body.pizzaSize.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Pizza Size không hợp lệ"
        })
    }
    if(body.pizzaType !== undefined && body.pizzaType.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Pizza type không hợp lệ"
        })
    }
    if(body.status !== undefined && body.status.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Status không hợp lệ"
        })
    }
    if(body.user !== undefined && body.user.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "user không hợp lệ"
        })
    }
   
    //bước 3: Gọi Model tạo dữ liệu
    const updateOrder = {
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        status: body.status,
        user: body.user
    }
   
    //bước 4: trả về kết quả 
    orderModel.findByIdAndUpdate(orderId, updateOrder,{new:true}, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update order successfully", 
            data: data
        })
    })
}
const deleteOrderById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderId không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete order successfully"
        })
    })
}
module.exports = {
    getAllOrder,
    createOrder,
    getOrderByID,
    updateOrderByID,
    deleteOrderById
}