//Import thư viện Mongoose
const mongoose =  require("mongoose");

//import model Drink vào file
const voucherModel = require("../models/voucherModel");

const createVoucher = (request, response) => {
    //bước 1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "maVoucher" : "123123",
    //     "phanTramGiamGia" : 15,
    //     "ghiChu" : "Hello"
    // }
    //bước 2: validate dữ liệu
    //kiểm tra maVoucher có hợp lệ hay không 
    if(!body.maVoucher){
        return response.status(400).json({
            status: "Bad Request",
            message: "maVoucher không hợp lệ",
        })
    }
    //kiểm tra phanTramGiamGia 
    if((isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0) && !body.phanTramGiamGia){
        return response.status(400).json({
            status: "Bad Request",
            message: "maGiamGia không hợp lệ",
        })
    }
    
    //bước 3: Gọi Model tạo dữ liệu
    const newVoucher = {
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }
    //bước 4: Trả về kết quả 
    voucherModel.create(newVoucher,(error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return response.status(201).json({
            status: "Create voucher: Successfull",
            data:  data
        })
    });
}
const getAllvoucher = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    // bước 4: Trả về kết quả
    voucherModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher: Successfull",
            data: data
        })
    })
}
const getVoucherById = (request,response) =>{
    // bước 1: chuẩn bị dữ liệu
   const voucherId  =  request.params.voucherId;
   //bước 2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(voucherId)){
    return response.status(400).json({
            status: "Bad request",
            message: "Voucher không hợp lệ" 
        })
    }
    //bước 3 - 4: gọi model tạo dữ liêu  và trả về 
    voucherModel.findById(voucherId,(error,data) =>{
        return response.status(200).json({
            status: "Get Detail Voucher successfull",
            data: data
            })
        }
    )  
}
const updateVoucherById = (request, response) =>{
    //bước 1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body; //lấy thông tin từ json
 
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
         return response.status(400).json({
             status: "Bad request",
             message: "voucherId không hợp lệ" 
         })
    } 
    if(body.maVoucher !== undefined && body.maVoucher.trim() === ""){
         return response.status(400).json({
             status: "Bad Request",
             message: "maVoucher không hợp lệ"
         })
    }
    if(body.phanTramGiamGia !== undefined && (isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0) ){
        return response.status(400).json({
            status: "Bad Request",
            message: "phanTramGiamGia không hợp lệ"
        })
    }
    if(body.ghiChu !== undefined && body.ghiChu.trim() === ""){
     return response.status(400).json({
         status: "Bad Request",
         message: "ghiChu không hợp lệ"
     })
    }
   
     //bước 3: Gọi model tạo dữ liệu
     const updateVoucher= {};
 
     if(body.maVoucher !== undefined){
        updateVoucher.maVoucher = body.maVoucher;
     }
     if(body.phanTramGiamGia !== undefined){
        updateVoucher.phanTramGiamGia = body.phanTramGiamGia;
     }
     if(body.ghiChu !== undefined){
        updateVoucher.ghiChu = body.phanTramGiamGia;
     }
      //bước 4: trả về kết quả 
      voucherModel.findByIdAndUpdate(voucherId, updateVoucher,{new: true}, (error, data) => {
         if(error){
             return response.status(500).json({
                 status: "Internal server error",
                 message: error.message
             })
         }
         return response.status(200).json({
             status: "Update vocuher successfully", 
             data: data
         })
     })
 }
 const deleteVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherID = request.params.voucherId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Delete voucher successfully"
        })
    })
}
module.exports = {
    createVoucher,
    getAllvoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}