//Import thư viện Mongoose
const mongoose =  require("mongoose");

// import model cần dùng
const userModel = require("../models/userModel")
const orderModel = require("../models/orderModel")
const voucherModel = require("../models/voucherModel")

const createOrder = function(request, response) {
    // b1 chuẩn bị dữ liệu
    let body = request.body;
    // b2 kiểm tra
    if(!body.email) {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    } else {
        userModel.findOne({ email: body.email}, function(error, foundUser){
            if (error) { return response.status(500).json({
                status: "Internal server error",
                message: error.message 
            })} else if (!foundUser) { 
                // tiếp tục validate dữ liệu
                if(!body.hoTen){
                    return response.status(400).json({
                        status: "BAD REQUEST",
                        message: "hoTen không hợp lệ" 
                    })
                }
                if(!body.diaChi){
                    return response.status(400).json({
                        status: "BAD REQUEST",
                        message: "diaChi không hợp lệ" 
                    })
                }
                if(!body.soDienThoai){
                    return response.status(400).json({
                        status: "BAD REQUEST",
                        message: "Phone không hợp lệ" 
                    })
                }
                const newUser = {
                    fullName: body.hoTen,
                    email: body.email,
                    address: body.diaChi,
                    phone: body.soDienThoai
                };
                // tạo user mới 
                userModel.create(newUser, (error, createdUser) => {
                    if (error) {
                        return response.status(500).json({
                            status: "Internal server error",
                            message: error.message
                        })
                    } else { // tạo user thành công, dùng user đó để tạo order
                        let newOrder = {
                            pizzaSize: body.pizzaSize,
                            pizzaType: body.pizzaType,
                            status: body.status,
                            user: createdUser._id
                        }
                        orderModel.create(newOrder, (error, createdOrder) => {
                            if(error){
                                return response.status(500).json({
                                    status: "Internal server error",
                                    message: error.message
                                })
                            }
                            return response.status(201).json({
                                status: "Create order successfully",
                                data: createdOrder
                            })
                        } )
                    }
                } );
            } else { 
                let newOrder = {
                    pizzaSize: body.pizzaSize,
                    pizzaType: body.pizzaType,
                    status: body.status,
                    user: foundUser._id
                }
                orderModel.create(newOrder, (error, createdOrder) => {
                    if(error){
                        return response.status(500).json({
                            status: "Internal server error",
                            message: error.message
                        })
                    }
                    return response.status(201).json({
                        status: "Create order successfully",
                        data: createdOrder
                    })
                } )
            }
        })
    }
}

const getVoucherDetail = function (request, response) {
    // b1 chuẩn bị data
    let maVoucher = request.params.maVoucher
    // b2 kiểm tra
    if(!maVoucher) {
        return response.json({
        })
    }
    voucherModel.findOne({ maVoucher : maVoucher}).exec(function(error, foundVoucher) {
        return response.json(foundVoucher)
    }) 
}
module.exports = { 
    createOrder,
    getVoucherDetail
 }