/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// biến toàn cục combo đã chọn 
var gChosenCombo = "";

// biến dữ liệu combo để tiện quá trình lập trình
var gCombo = {
    "" : {  detail : "" ,
            object: {},
            price: 0 },
    "S" : { detail : "Menu S, đường kính pizza: 20cm, sườn nướng: 2, salad: 200g, nước ngọt: 2" ,
            object: {
                kichCo: "S",
                duongKinh: "20",
                suon: "2",
                salad: "200",
                soLuongNuoc: "2",
                thanhTien: "150000"
            },
            price: 150000 },
    "M" : { detail : "Menu M, đường kính pizza: 25cm, sườn nướng: 4, salad: 300g, nước ngọt: 3" ,
            object: {
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                soLuongNuoc: "3",
                thanhTien: "200000"
            },
            price: 200000 },
    "L" : { detail : "Menu L, đường kính pizza: 30cm, sườn nướng: 8, salad: 500g, nước ngọt: 4" , 
            object: {
                kichCo: "L",
                duongKinh: "30",
                suon: "8",
                salad: "500",
                soLuongNuoc: "4",
                thanhTien: "250000"
            },
            price: 250000 },
}
// biến toàn cục pizza đã chọn 
var gChosenPizza = "";

// biến toàn cục phần trăm giảm giá 
var gDiscountPercentage = 0;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(function() {
    onPageLoading();
    $(".btn-select-combo").click(onBtnSelectComboClick);
    $(".btn-select-pizza").click(onBtnSelectPizzaClick);
    $("#btn-send-order").click(onBtnSendOrderClick);
    $("#btn-create-order").click(onBtnCreateOrderClick)
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện load trang
function onPageLoading() {
    loadDrinksToDrinkSelect();
}
// hàm sử lý sự kiện ấn nút chọn combo
function onBtnSelectComboClick() {
    // bỏ class làm nút màu đỏ và trạng thái active của tất cả các nút combo
    $(".btn-select-combo").removeClass("btn-danger active");
    // thêm class làm nút màu đỏ và trạng thái active cho nút đã ấn
    $(this).toggleClass("btn-danger active");

    // lưu combo size (truy xuất từ card title) vào biến toàn cục và hiển thị trên console
    let comboSize = $(this).closest(".card").find(".card-title").eq(0).text()[0];
    gChosenCombo = comboSize;
    console.log("%cChọn combo : " + gChosenCombo, "color:blue");
}
// hàm sử lý sự kiện ấn nút chọn pizza
function onBtnSelectPizzaClick() {
    // bỏ class làm nút màu đỏ và trạng thái active của tất cả các nút pizza
    $(".btn-select-pizza").removeClass("btn-danger active");
    // thêm class làm nút màu đỏ và trạng thái active cho nút đã ấn
    $(this).toggleClass("btn-danger active");

    // lưu loại pizza (truy xuất từ card title) vào biến toàn cục và hiển thị trên console
    let pizzaName = $(this).closest(".card").find(".card-title").eq(0).text();
    gChosenPizza = pizzaName;
    console.log("%cChọn pizza : " + gChosenPizza, "color:blue");
}
// hàm sử lý sự kiện ấn nút gửi đơn
function onBtnSendOrderClick() {
    // hiện modal thông tin đơn
    $("#modal-order-info").modal("show");

    // điền thông tin lên thông tin order từ input đã điền.
    // mỗi input từ form có id tương ứng với ô trong modal nên dùng vòng lặp để điền
    $("#customer-info-form").find("input").each(function() {
        let propertyName = $(this).prop("id").slice(4);
        $("#order-info-form").find("#inp-order-" + propertyName).val($(this).val().trim());
    })

    // thêm thông tin chi tiết cho ô chi tiết
    detailMessage = getDetailMessage();
    $("#inp-order-detail").text(detailMessage);
    
}

function onBtnCreateOrderClick() {
    // thu thập dữ liệu
    var vObjectRequest = gCombo[gChosenCombo].object;
    vObjectRequest.loaiPizza = gChosenPizza;
    vObjectRequest.hoTen = $("#inp-order-name").val();
    vObjectRequest.email = $("#inp-order-email").val();
    vObjectRequest.soDienThoai = $("#inp-order-phone").val();
    vObjectRequest.diaChi = $("#inp-order-address").val();
    vObjectRequest.idLoaiNuocUong = $("#select-drink").val();
    vObjectRequest.loiNhan = $("#inp-order-message").val();
    vObjectRequest.idVourcher = $("#inp-order-voucher").val();
    console.log(JSON.stringify(vObjectRequest));
    // bỏ qua bước kiểm tra

    // gọi api POST dữ liệu mới vào database
    $.ajax({
        url : "/devcamp-pizza365/orders",
        type: "POST",
        contentType : "application/json;charset=UTF-8",
        dataType: "json",
        data: JSON.stringify(vObjectRequest),
        success: function(responseObject) {
            // nếu thành công thì ẩn modal thông tin order và hiện modal hiển thị mã order
            $("#modal-order-info").modal("hide");
            $("#modal-order-get-id").modal("show");

            // ghi ra console object phản hồi và điền vào ô mã order trong modal vừa hiển thị từ thông tin object nhận về
            console.log(responseObject);
            $("#inp-order-code").val(responseObject.data.orderCode);
        }
    })
}
/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm điền các đồ uống vào ô select đồ uống
function loadDrinksToDrinkSelect() {
    // gọi api GET dữ liệu các đồ uống
    $.ajax({
        url: "/devcamp-pizza365/drinks",
        dataType: "json",
        type: "GET",
        success: function(allDrinks) {
            // nếu thành công thì nhận được một array gồm các object đồ uống ở object phản hồi
            // dùng vòng lặp hiển thị các đồ uống vào trong select đồ uống
            allDrinks.data.forEach(drink => {
                $("<option>", {
                    val: drink.maNuocUong,
                    text: drink.tenNuocUong
                })
                .appendTo($("#select-drink"));
            });
        }
    })
}

// hàm tìm phần trăm giảm giá của voucher dựa vào voucher ID tham số
// gọi api GET dữ liệu voucher. hàm đồng bộ
function getDiscountbyID(paramVoucherId) {
    $.ajax({
        url: "/devcamp-pizza365/voucher_detail/" + paramVoucherId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function(voucher) {
            // nếu thành công thì sẽ trả về object thông tin của voucher tương ứng với id. 
            // lưu thông tin phần trăm giảm giá vào tham số toàn cục để xử lý
            gDiscountPercentage = voucher.phanTramGiamGia;
        },
        error: function() {
            // nếu thất bại thì coi như phần trăm giảm giá bằng 0
            gDiscountPercentage = 0;
        }
    })
}

// hàm trả về thông tin chi tiết để hiển thị trên modal
function getDetailMessage() {
    // lấy được id voucher và phần trăm giảm giá
    let inputVoucher = $("#inp-order-voucher").val();
    if (inputVoucher != "") { 
        getDiscountbyID(inputVoucher)
    };
    // thông tin khách hàng
    let customerInfo = [];
    if ($("#inp-order-name").val() != "" ) {
        customerInfo.push($("#inp-order-name").val())
    }
    if ($("#inp-order-email").val() != "" ) {
        customerInfo.push($("#inp-order-email").val())
    }
    if ($("#inp-order-phone").val() != "" ) {
        customerInfo.push($("#inp-order-phone").val())
    }
    if ($("#inp-order-address").val() != "" ) {
        customerInfo.push($("#inp-order-address").val())
    }

    var detailMessage = 
        "Xác nhận: " +
        // mỗi phần thông tin khách hàng cách nhau bằng dấu phẩy
        customerInfo.join(", ")
        + "\n" +

        // nếu chọn combo thì ghi thông tin combo ra, không thì để trống
        (   gChosenCombo != "" ?
            gCombo[gChosenCombo].detail +
            (
                $("#select-drink").val() != "" ?
                " " + $("#select-drink :selected").text():
                ""
            ) +
            "\n":
            ""
        ) + 
        // nếu chọn pizza thì ghi ra, không thì để trống
        (   gChosenPizza != "" ?
            "Loại pizza: " + gChosenPizza + ". " :
            ""
        ) + 
        // giá
        "Giá: " + gCombo[gChosenCombo].price + " vnd"
        ;
        // nếu có mã giảm giá thì ghi ra
        if (gDiscountPercentage != 0) {
            detailMessage += ", Mã giảm giá: " + inputVoucher;
        }
        detailMessage += "\n"
        // tính tổng phải thanh toán tuỳ thuộc vào nếu có mã giảm giá
        + "Phải thanh toán: " + gCombo[gChosenCombo].price * (100-gDiscountPercentage)/100 + " vnd";
        if (gDiscountPercentage != 0) {
            detailMessage += " (giảm giá " + gDiscountPercentage +"%)";
        }
        
    return detailMessage;
}